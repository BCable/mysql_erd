#!/bin/bash

#
# MySQL_ERD
#
# Author: Brad Cable
# Email: brad@bcable.net
# License: MIT
#

# parameter parsing {{{

database=""
dotfile=""
host=""
format=""
password=""
username=""
outfile=""

while [ ! -z "$1" ]; do
	case "$1" in
		"-d")
			dotfile="true"
			shift ;;
		"-f")
			format="$2"
			shift 2 ;;
		"-h")
			host="$2"
			shift 2 ;;
		"-o")
			outfile="$2"
			shift 2 ;;
		"-u")
			username="$2"
			shift 2 ;;
		"-p")
			password="yes"
			shift ;;
		*)
			database="$1"
			shift ;;
	esac
done

if [ -z "$database" ]; then
	echo No database specified. 1>&2
	exit 1
fi

if [ -z "$password" ] || [ "$password" = "yes" ]; then
	stty -echo
	read -p "Password: " password
	stty echo
fi

if [ -z "$outfile" ]; then
	outfile="$database.$format"
fi

# }}}

# helper functions {{{

function mysql_exe(){
	mysql --host "$host" -u "$username" --password="$password" "$database" -t -e "$1"
}

# }}}

# table parsing {{{

function parse_table(){
	stdin="`cat /dev/stdin`"
	lines="`echo "$stdin" | wc -l`"
	let sub1=$lines-1
	table="`echo "$stdin" | head -n "$sub1"`"
	let sub1-=1
	table="`echo "$table" | tail -n "$sub1"`"
	table="`echo "$table" | sed -r "s/^.(.*)[\+\|].*?$/\1/g"`"
	table="`echo "$table" | sed ":a;/$/{N;s/[\r\n]/\\\\\\n/;ba}"`"
	echo "$table"
}

function build_table_code(){
	echo -n "$1[label=\"$1\n"
	mysql_exe "explain $1;" | parse_table | sed -r "s/$/\"];/"
}

# }}}

# table data {{{

function get_constraints(){
	mysql_exe "show create table $1;" | grep CONSTRAINT
}

function get_tables(){
	mysql_exe "show tables;" | grep "|" | grep -v "| Tables_in_" | \
		sed -r "s/^\| ([^ ]*) .*$/\1/g"
}

# }}}

# graphviz erd building {{{

function out_connections(){
	get_constraints "$1" | sed -r "s/^.*REFERENCES .([^ ]*)[^ ] .*$/$1 -> \1;/g"
}

function graphviz_header(){
	echo "strict digraph erd {"
	echo "node [shape=box, fontname=\"monospace\", fontsize=\"8\"];"
	echo
}

function graphviz_footer(){
	echo "}"
}

function build_erd(){
	graphviz_header

	get_tables | while read table; do
		out_connections "$table"
		build_table_code "$table"
	done

	graphviz_footer
}

# }}}

if [ ! -z "$dotfile" ]; then
	build_erd
else
	build_erd | dot -T$format /dev/stdin > $outfile
fi
